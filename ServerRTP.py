#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

"""Programa realizado por Alberto García Zafra"""

import socketserver
import threading
import sys
import simplertp
import time

try:
    ServerSIP = sys.argv[1]
    IPPORT = ServerSIP.split(":")
    IPServerSIP = IPPORT[0]
    PortServerSIP = int(IPPORT[1])

    Service = sys.argv[2]
    File = sys.argv[3]

except IndexError:
    print("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>")
except ValueError:
    print("Usage: python3 serverrtp.py <IPServerSIP>:<portServerSIP> <service> <file>")


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f"{year}{month}{day}{hour}{minute}{second}"


class SIPHandler(socketserver.BaseRequestHandler):
    IPPORT_client = []

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]

        print(f"{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}: {data.decode('utf-8')}")

        """Received = data.decode('utf-8')
        Method = Received.split()[0]
        Method = Method.upper()"""

        Received = data.decode('utf-8')
        Method = Received.split()[0]
        Method = Method.upper()

        if Method == "SIP/2.0":
            pass
        elif Method == "INVITE":
            headers = Received.splitlines()[4:]
            if len(headers) > 0:
                originclient = headers[1]
                ClientIP = originclient.split()[1]
                clientmedia = headers[4]
                Clientrtpport = clientmedia.split()[1]

                self.IPPORT_client.append(ClientIP)
                self.IPPORT_client.append(Clientrtpport)

            sock.sendto(f"SIP/2.0 200 OK\r\n\r\n".encode(), self.client_address)
            print(f"{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: SIP/2.0 200 OK\r\n\r\n")

        elif Method == "ACK":
            print(self.IPPORT_client)
            sender = simplertp.RTPSender(ip=self.IPPORT_client[0], port=int(self.IPPORT_client[1]), file=File,
                                         printout=True)
            sender.send_threaded()
            time.sleep(10)
            sender.finish()

        elif Method == "BYE":
            sock.sendto(f"SIP/2.0 200 OK\r\n\r\n".encode(), self.client_address)
            print(f"{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: SIP/2.0 200 OK\r\n\r\n")


def main():
    # Listens at port (my address)
    # and calls the SIPRegisterHandlers class to manage the request

    Register = f"REGISTER sip:{Service}@songs.net SIP/2.0\r\nExpires: 62\r\n\r\n"

    try:
        serv = socketserver.UDPServer(('0.0.0.0', 0), SIPHandler)
        print(f"{get_time()} Starting...")
        # Registramos con el socket que usará el servidor SIP
        rtp_port = serv.server_address[1]
        serv.socket.sendto(Register.encode('utf-8'), (IPServerSIP, PortServerSIP))
        print(f"{get_time()} SIP to {IPServerSIP}:{PortServerSIP}: {Register}")
        time.sleep(30)
        serv.socket.sendto(Register.encode('utf-8'), (IPServerSIP, PortServerSIP))
        print(f"{get_time()} SIP to {IPServerSIP}:{PortServerSIP}: {Register}")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()

