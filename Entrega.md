## Parte Básica:

    Todo funciona correctanete y hace lo que debería, he seguido todas los pasos de la práctica y no hay errores.
    Lo único que tanto el servidor SIP como el RTP hay que interrumpirlos con control-c.
    Los nombres de los programas son Client.py, ServerSIP.py y ServerRTP.py

## Parte Adicional:

  *Cabecera de tamaño
     
     He usado content-lenght como cabecera de la misma forma que se hizo en la práctica 4, 
     para calcular el contenido del cuerpo, simplemente he pasado el contenido a bytes y con la función len(),
     he hallado el número de bytes.
  
  *Tiempo de expiración
     
     He añadido la cabecera Expires con los segundos de expiración, como vimos en la práctica 4 para los métodos REGISTER.
     Para que renueve el registro a los 30 segundos, enviando otra petición, he mandado esperar al servidor RTP con time.sleep.
     Una vez pasados los 30 segundos, el servidor RTP manda un nuevo REGISTER, renovando con ello el registro y el programa continúa.
  
  *Gestión de errores
    
     Para la gestión de errores, lo principal es que los argumentos pasados por línea de comandos sean los correctos.
     He capturado excepciones en caso de que no se introduzcan el número de valores correcto, no sean válidos o el formato sea erróneo.
     También, en caso de que el cliente o el servidor RTP envíen peticiones al servidor SIP con cualquier otro método que no sea INVITE, ACK o REGISTER,
     mandará un mensaje de método no permitido. También mandará un mensaje de formato erróneo si los usuarios SIP no están escritos correctamente.
     Por último si el servidor RTP no se encuentra registrado cuando el cliente le manda la petición INVITE al servidor SIP mandará un mensaje SIP de error, usuario no encontrado.


    
     