#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente UDP que abre un socket a un servidor
"""

"""Programa realizado por Alberto García Zafra"""

import sys
import socket
import socketserver
import time
import threading

IP = '127.0.0.1'
PORT = 34543


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f"{year}{month}{day}{hour}{minute}{second}"


try:
    ServerSIP = sys.argv[1]
    IPPORT = ServerSIP.split(":")
    IPServerSIP = IPPORT[0]
    PortServerSIP = int(IPPORT[1])

    AddrClient = sys.argv[2]
    Correct = AddrClient.split(":")
    CorrectSIP = Correct[0]
    if CorrectSIP != "sip":
        sys.exit(
            "Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")

    AddrServerRTP = sys.argv[3]
    Correct2 = AddrServerRTP.split(":")
    ServerUser = Correct2[1]
    Sessionname = ServerUser.split("@")[0]
    CorrectSIP2 = Correct2[0]
    if CorrectSIP2 != "sip":
        sys.exit(
            "Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")

    Time = int(sys.argv[4])
    File = sys.argv[5]

except IndexError:
    sys.exit("Usage: python3 client.py <IPServerSIP>:<portServerSIP> <addrClient> <addrServerRTP> <time> <file>")
class RTPHandler(socketserver.BaseRequestHandler):
    """Clase para manejar los paquetes RTP que nos lleguen.

    Atención: Heredamos de BaseRequestHandler porque UDPRequestHandler
    siempre termina enviando una respuesta al cliente, algo que aquí no
    queremos hacer. Al usar BaseRequestHandler no podemos usar self.rfile
    (porque esta clase no lo tiene), así que leemos el mensaje directamente
    de self.request (que es una lista, cuyo primer elemento es el mensaje
    recibido)"""

    def handle(self):
        msg = self.request[0]
        # sólo nos interesa lo que hay a partir del byte 54 del paquete UDP
        # que es donde está la payload del paquete RTP qeu va dentro de él
        payload = msg[12:]
        # Escribimos esta payload en el fichero de salida
        self.output.write(payload)
        print("Received")

    @classmethod
    def open_output(cls, filename):
        """Abrir el fichero donde se va a escribir lo recibido.

        Lo abrimos en modo escritura (w) y binario (b)
        Es un método de la clase para que podamos invocarlo sobre la clase
        antes de empezar a recibir paquetes."""

        cls.output = open(filename, 'wb')

    @classmethod
    def close_output(cls):
        """Cerrar el fichero donde se va a escribir lo recibido.

        Es un método de la clase para que podamos invocarlo sobre la clase
        después de terminar de recibir paquetes."""
        cls.output.close()


def main():

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f"{get_time()} Starting...")

            # Petición INVITE a servidor SIP
            Request = "INVITE" + " " + AddrServerRTP + " SIP/2.0\r\n"

            # Cabecera
            header = "application/sdp"


            # Definimos los parámetros en formato SDP
            version = "0"
            origin = AddrClient + " 127.0.0.1"
            sessionname = Sessionname
            tiempo = "0"
            rtp_port = "34543"
            media = "audio " + rtp_port + " RTP"

            body = f"v={version}\r\no={origin}\r\ns={sessionname}\r\nt={tiempo}\r\nm={media}\r\n"

            header2 = len(bytes(body, 'utf-8'))

            # Construimos la petición INVITE con el SDP
            Invite = f"{Request}Content-type:{header}\r\nContent-lenght:{header2}\r\n\r\n{body}"
            print(Invite)
            # Mandamos la petición
            my_socket.sendto(Invite.encode('utf-8'), (IPServerSIP, PortServerSIP))
            print(f"{get_time()} SIP to {IPServerSIP}:{PortServerSIP}: {AddrServerRTP}")

            # Recibimos respuesta
            ResponseServerSIP = my_socket.recv(1024).decode('utf-8')
            print(f"{get_time()} SIP from {IPServerSIP}:{PortServerSIP}: {ResponseServerSIP}")

            Response2 = ResponseServerSIP.splitlines()
            ContactRTP = Response2[1]
            newContact = ContactRTP.split(" ")[1]
            IPPortServerRTP = newContact.split(":")[1:]
            ServerRTPUser = IPPortServerRTP[0]
            IPServerRTP = ServerRTPUser.split("@")[1]
            PortServerRTP = int(IPPortServerRTP[1])

            # Se envía ACK al servidor SIP
            if Response2[0] == "SIP/2.0 302 Moved Temporarily":
                ack = f"ACK {AddrServerRTP} SIP/2.0\r\n\r\n"
                print("Sending ACK")
                my_socket.sendto(ack.encode('utf-8'), (IPServerSIP, PortServerSIP))
                print(f"{get_time()} SIP to {IPServerSIP}:{PortServerSIP}: {ack}")

                # Construimos la petición INVITE para el serverRTP
                Request2 = "INVITE" + " " + newContact + " SIP/2.0\r\n"
                Invite2 = f"{Request2}Content-type:{header}\r\nContent-lenght:{header2}\r\n\r\n{body}"
                print(Invite2)
                my_socket.sendto(Invite2.encode('utf-8'), (IPServerRTP, PortServerRTP))
                print(f"{get_time()} SIP to {IPServerRTP}:{PortServerRTP}: {Request2}")

            # Primer mensaje de respuesta del servidor RTP
            Response1RTP = my_socket.recv(1024).decode('utf-8')
            print(f"{get_time()} SIP from {IPServerRTP}:{PortServerRTP}: {Response1RTP}")
            Response1RTP = Response1RTP.splitlines()

            # Si recibimos 200 OK del servidorRTP le manda un ACK al servidorSIP
            if Response1RTP[0] == "SIP/2.0 200 OK":
                ack = f"ACK {newContact} SIP/2.0\r\n\r\n"
                print("Sending ACK")
                my_socket.sendto(ack.encode('utf-8'), (IPServerRTP, PortServerRTP))
                print(f"{get_time()} SIP to {IPServerRTP}:{PortServerRTP}: {ack}")

                RTPHandler.open_output(File)
                with socketserver.UDPServer((IP, PORT), RTPHandler) as serv:
                    print("Listening...")
                    # El bucle de recepción (serv_forever) va en un hilo aparte,
                    # para que se continue ejecutando este programa principal,
                    # y podamos interrumpir ese bucle más adelante
                    threading.Thread(target=serv.serve_forever).start()
                    # Paramos un rato. Igualmente podríamos esperar a recibir BYE,
                    # por ejemplo
                    time.sleep(Time)
                    print("Time passed, shutting down receiver loop.")
                    # Paramos el bucle de recepción, con lo que terminará el thread,
                    # y dejamos de recibir paquetes
                    serv.shutdown()
                # Cerramos el fichero donde estamos escribiendo los datos recibidos
                RTPHandler.close_output()

                # Enviamos BYE para terminar la comunicación
                bye = f"BYE {newContact} SIP/2.0\r\n\r\n"
                my_socket.sendto(bye.encode('utf-8'), (IPServerRTP, PortServerRTP))
                print(f"{get_time()} SIP to {IPServerRTP}:{PortServerRTP}: {bye}")

                # OK del servidorRTP a terminar la comunicación
                Response2RTP = my_socket.recv(1024).decode('utf-8')
                print(f"{get_time()} SIP from {IPServerRTP}:{PortServerRTP}: {Response2RTP}")
                Response2RTP = Response2RTP.splitlines()
                if Response2RTP[0] == "SIP/2.0 200 OK":
                    my_socket.close()

    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()

