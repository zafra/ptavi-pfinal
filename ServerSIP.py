#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

"""Programa realizado por Alberto García Zafra"""

import socketserver
import sys
import json
import time

try:
    PORT = int(sys.argv[1])
except IndexError:
    print("Usage: python3 ServerRTP.py <port>")
except ValueError:
    print("Usage: python3 ServerRTP.py <port>")


def get_time():
    actual_time = time.localtime()
    year = str(actual_time.tm_year).zfill(2)
    month = str(actual_time.tm_mon).zfill(2)
    day = str(actual_time.tm_mday).zfill(2)
    hour = str(actual_time.tm_hour).zfill(2)
    minute = str(actual_time.tm_min).zfill(2)
    second = str(actual_time.tm_sec).zfill(2)

    return f"{year}{month}{day}{hour}{minute}{second}"


class SIPRequest:

    def __init__(self, data):
        self.data = data.decode('utf-8')

    def parse(self):
        self._parse_command(self.data)
        headers = self.data.splitlines()[4:]
        self._parse_headers(headers)

    def _get_address(self, uri):
        self.uri = uri
        listuri = self.uri.split(":")
        schema = listuri[0]
        address = listuri[1]
        return address, schema

    def _parse_command(self, line):
        listdata = line.split()
        self.command = listdata[0].upper()
        self.uri = listdata[1]
        uri = self._get_address(self.uri)
        self.address = uri[0]
        usernameIP = self.address.split("@")
        self.username = usernameIP[0]
        self.IP = usernameIP[1]
        schema = uri[1]

        if self.command == "REGISTER" and schema == "sip":
            self.result = "200 OK"
        elif self.command == "INVITE" and schema == "sip":
            self.result = "302 Moved Temporarily"
        elif self.command != "REGISTER" or self.command != "INVITE" or self.command != "ACK" or self.command != "BYE":
            self.result = "405 Method not allowed"
        elif schema != "sip":
            self.result = "400 Bad format"

    def _parse_headers(self, first_nl):
        if len(first_nl) > 0:
            self.version = first_nl[0]
            originclient = first_nl[1]
            self.clientIP = originclient.split()[1]
            self.sessionname = first_nl[2]
            self.time = first_nl[3]
            clientmedia = first_nl[4]
            self.clientrtpport = clientmedia.split()[1]


class SIPRegisterHandler(socketserver.BaseRequestHandler):
    Userregistered = {}

    def process_register(self, sip, real):
        self.Userregistered[sip] = real
        with open("registered.json", "w") as file:
            json.dump(self.Userregistered, file, indent=4)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]

        print(f"{get_time()} SIP from {self.client_address[0]}:{self.client_address[1]}: {data.decode('utf-8')}")

        sip_request = SIPRequest(data)
        sip_request.parse()
        ServiceAddress = sip_request.uri
        RealAdress = f"{ServiceAddress.split('@')[0]}@{self.client_address[0]}:{self.client_address[1]}"

        if (sip_request.command == "REGISTER") and (sip_request.result == "200 OK"):
            self.process_register(ServiceAddress, RealAdress)
            sock.sendto(f"SIP/2.0 {sip_request.result}\r\n\r\n".encode(), self.client_address)
            print(f"{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: SIP/2.0 {sip_request.result}\r\n\r\n")
        elif (sip_request.command == "INVITE") and (sip_request.result == "302 Moved Temporarily"):
            if ServiceAddress in self.Userregistered:
                redirect = f"SIP/2.0 {sip_request.result}\r\nContact: {self.Userregistered[ServiceAddress]}\r\n\r\n"
                sock.sendto(redirect.encode(), self.client_address)
                print(f"{get_time()} SIP to {self.client_address[0]}:{self.client_address[1]}: {redirect}")
            else:
                sock.sendto(f"SIP/2.0 404 Not found\r\n\r\n".encode(), self.client_address)
        elif sip_request.command == "ACK":
            pass


def main():
    # Listens at port (my address)
    # and calls the SIPRegisterHandlers class to manage the request
    try:
        serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)
        print(f"{get_time()} Starting...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
        sys.exit(0)


if __name__ == "__main__":
    main()
